from flask import Flask, request, jsonify
import spacy
from spacy.util import minibatch, compounding

nlp2 = spacy.load('custom1')

app = Flask(__name__)

@app.route("/analyze", methods=['POST'])
def home():
    entities = []
    req_data = request.get_json()
    doc2 = nlp2(req_data['text'])

    for ent in doc2.ents:
        entities.append({'label': ent.label_, 'text': ent.text})
        # print(ent.label_, ent.text)
    return jsonify(entities)
    
if __name__ == "__main__":
    app.run(debug=True)