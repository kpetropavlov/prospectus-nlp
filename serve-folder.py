import os
import io

import spacy
from spacy.util import minibatch, compounding

nlp2 = spacy.load('custom1')
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
import xlsxwriter 

arr = os.listdir('docs')
# print(arr)

def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh, 
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)
        text = fake_file_handle.getvalue()
    # close open handles
    converter.close()
    fake_file_handle.close()
    if text:
        return text

workbook = xlsxwriter.Workbook('result.xlsx') 

worksheet = workbook.add_worksheet() 
row = 1
column = 0

worksheet.write(0, 0, 'Document')
worksheet.write(0, 1, 'Equity Interest Purchase Undertaking') 

for doc in arr:
    doc2 = nlp2(extract_text_from_pdf('docs/' + doc))
    entities = []
    for ent in doc2.ents:
        entities.append({'label': ent.label_, 'text': ent.text})
        # print(ent.label_, ent.text)
    worksheet.write(row, 0, doc)
    if len(entities) > 0:
        worksheet.write(row, 1, 'Yes')
    else:
        worksheet.write(row, 1, 'No')
    row = row + 1
    print(doc + ' has been processed')
workbook.close() 
